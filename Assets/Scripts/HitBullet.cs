﻿using System.Collections;
using UnityEngine;

public class HitBullet : MonoBehaviour {
    [SerializeField] private Transform  shootPoint;
    [SerializeField] private GameObject _bullet;

    private float m_MaxSpeed = 3f;

    public void Shoot() {
        Transform   bullet = CreateBullet();
        Rigidbody2D rig    = bullet.gameObject.GetComponent<Rigidbody2D>();
        Vector2     dir    = shootPoint.right;
        StartCoroutine(MoveBullet(dir, rig));
    }

    private Transform CreateBullet() {
        Transform bullet = Instantiate(_bullet.transform);
        bullet.transform.position = shootPoint.transform.position;
        return bullet;
    }

    // private Vector2 GetTargetPoint() {
    //     return transform.position;
    // }

    private IEnumerator MoveBullet(Vector2 direction, Rigidbody2D rig) {
        Vector2 velocity       = rig.velocity;
        Vector2 startDirection = direction;
        while(startDirection.x < direction.x + 3f) {
            velocity.x       =  direction.x * m_MaxSpeed;
            rig.velocity     =  velocity;
            startDirection.x += 0.01f;
            yield return null;
        }

        if(rig != null) {
            Destroy(rig.gameObject);
        }
    }
}
