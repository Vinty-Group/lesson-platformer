﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : CharacterMovement {
    [SerializeField] private float          m_MaxSpeed = 10f;
    [SerializeField] private SpriteRenderer m_Graphics;
    [SerializeField] private float          m_JumpForce = 5f;
    [SerializeField] private GameObject     m_GroundPoint;
    [SerializeField] private Transform      m_ShootPointsRoot;
    [SerializeField] private Animator       m_Animator;

    private Rigidbody2D rig;
    private Vector2     direction;

    private void Start() {
        rig                        =  GetComponent<Rigidbody2D>();
        InputController.JumpAction += Jump;
    }

    private void OnDestroy() {
        InputController.JumpAction -= Jump;
    }

    private void FixedUpdate() {
        if(IsFreezing) {
            Vector2 vel = rig.velocity;
            vel.x        = 0;
            rig.velocity = vel;
            return;
        }

        direction = DirectionHorizont();

        if(!IsGround()) {
            direction *= 0.5f;
        }

        Move(direction);
    }

    private bool IsGround() {
        Vector2 point = m_GroundPoint.transform.position;
        point.y -= 0.1f; // что бы избежать пересечения со своими коллапйдерами
        RaycastHit2D hit = Physics2D.Raycast(point, Vector2.down, 0.2f);
        return hit.collider != null;
    }

    public static Vector2 DirectionHorizont() {
        Vector2 direction = new Vector2(InputController.HorizontalAxis, 0f);
        return direction;
    }

    private void Update() {
        bool isMovement = Mathf.Abs(rig.velocity.x) > 0.01f;
        m_Animator.SetInteger("Speed", isMovement ? 1 : 0);

        if(Mathf.Abs(rig.velocity.x) < 0.01f) {
            return;
        }

        var isRight = rig.velocity.x > 0f;
        m_Graphics.flipX = !isRight;

        var angles = m_ShootPointsRoot.localEulerAngles;
        angles.y                           = isRight ? 0 : 180;
        m_ShootPointsRoot.localEulerAngles = angles;
    }

    public override void Move(Vector2 direction) {
        Vector2 velocity = rig.velocity;
        velocity.x   = direction.x * m_MaxSpeed;
        rig.velocity = velocity;
    }

    public override void Stop(float timer) {
    }

    public override void Jump(float force) {
        rig.AddForce(new Vector2(0f, force * m_JumpForce), ForceMode2D.Impulse);
    }
}
