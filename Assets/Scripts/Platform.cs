﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
   

    private void OnTriggerEnter2D(Collider2D other)
    {
        bool isMovementObject = other.transform.gameObject.GetComponent<CharacterMovement>();
        if (isMovementObject)
        {
            other.transform.parent = transform;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.parent == transform)
        {
            other.transform.parent = null;
        }
    }
}
