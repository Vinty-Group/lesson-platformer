﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObject : MonoBehaviour, IHitBox
{
    [SerializeField] private LevelObjectData m_Data;
    private int health = 1;

    public int Health
    {
        get => health;
        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        print(" Я Статик и я Умер!");
        Destroy(gameObject);
    }

    private void Start()
    {
        health = m_Data.Health;
        var rig = GetComponent<Rigidbody2D>();
        if (rig != null)
        {
            rig.bodyType = m_Data.Kinematic 
                ? RigidbodyType2D.Kinematic 
                : 
                RigidbodyType2D.Dynamic;
        }
    }
    
    [ContextMenu("Rename this object")]
    private void Rename()
    {
        if (m_Data)
        {
            gameObject.name = m_Data.Name;
        }
    }
}
