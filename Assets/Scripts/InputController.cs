﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    public static float          HorizontalAxis;
    public static Action<float>  JumpAction;
    public static Action<string> FireAction;

    [SerializeField] private HitBullet hit;

    private float     jumpTimer;
    private Coroutine waitJumpCoroutine;

    private void Start() {
        HorizontalAxis = 0;
        hit            = FindObjectOfType<HitBullet>();
    }

    private void Update() {
        HorizontalAxis = Input.GetAxis("Horizontal");
        if(Input.GetButtonDown("Jump")) {
            if(waitJumpCoroutine == null) {
                waitJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }

            jumpTimer = Time.time;
        }

        if(Input.GetButtonDown("Fire1")) {
            hit.Shoot();
            FireAction?.Invoke("Fire1");
        }

        if(Input.GetButtonDown("Fire2")) {
            FireAction?.Invoke("Fire2");
        }
    }

    private IEnumerator WaitJump() {
        yield return new WaitForSeconds(0.2f);
        if(JumpAction != null) {
            // если = 1 то прыжок будет 1
            float force = Time.time - jumpTimer < 0.2f ? 1.25f : 1f;
            JumpAction.Invoke(force);
        }

        waitJumpCoroutine = null;
    }
}
