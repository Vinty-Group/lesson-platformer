﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState {
    MainMenu,
    Loading,
    Game,
    GamePause
}

public class GameManager : MonoBehaviour {
    private static GameState currentGameState;
    public static  GameState CurrentGameState => currentGameState;

    public static Action<GameState> GameStateAction;

    public IPlayer      Player;
    public List<IEnemy> Enemies = new List<IEnemy>();

    public static void SetGameState(GameState newState) {
        currentGameState = newState;
        if(GameStateAction != null) {
            GameStateAction.Invoke(newState);
        }
    }

    private void Start() {
        print("Start...");
        SetGameState(GameState.Game);
        // print(Player);
        // print($"{Enemies.Count} enemies");
        //    StartCoroutine(WaitAndPrintStart());
    }

    private IEnumerator WaitAndPrintStart() {
        yield return new WaitForSeconds(1f);
        // print("Coroutine is work!");
    }
}
