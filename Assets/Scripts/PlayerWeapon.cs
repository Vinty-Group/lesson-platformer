﻿using System;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour, IDamager {
    [SerializeField] private WeaponData m_WeaponData;
    [SerializeField] private string     m_Axis = "Fire1";
    [SerializeField] private Transform  m_ShootPoing;

    public string Axis   => m_Axis;
    public int    Damage => m_WeaponData.Damage;

    public void SetDamage() {
        GameObject target = GetTarget();
        if(target != null)
        {
            var hitBox = target.GetComponent<IHitBox>();
            hitBox?.Hit(Damage);
        }
    }

    private GameObject GetTarget() {
        GameObject      target = null;
        var          point  = m_ShootPoing.position;
        var          dir    = m_ShootPoing.right;
        var          range  = m_WeaponData.Range;
        RaycastHit2D hit    = Physics2D.Raycast(point, dir, range);
        if(hit.collider != null) {
            target = hit.transform.gameObject;
        }
        return target;
    }
}
