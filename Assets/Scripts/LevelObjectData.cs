﻿using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName =  "Data", menuName =  "Object/LevelObject", order = 1)]
public class LevelObjectData : ScriptableObject
{
    public string Name = "New level object";
    [FormerlySerializedAs("Static")] public bool Kinematic;
    public int Health = 1;
    
    
}
