﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Object/Weapon", order = 1)]
public class WeaponData : ScriptableObject {
    public string WeaponName = "Weapon Name";
    public int Damage = 1;
    public int Range = 4;
    public float Rate = 1f;
    public GameObject Bullet;
    public float Speed;
}
