﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour, IPlayer, IHitBox {
    [SerializeField] private int            health = 1;
    [SerializeField] private PlayerWeapon[] m_Weapon;
    

    public int Health {
        get => health;
        private set {
            health = value;
            if(health <= 0) {
                Die();
            }
        }
    }

    public void Hit(int damage) {
        Health -= damage;
    }

    public void Die() {
        print(" Я Умер!");
    }

    private void Awake() {
        RegisterPlayer();
    }

    private void Start() {
        m_Weapon = GetComponents<PlayerWeapon>().ToArray();
        InputController.FireAction += Attack;
    }

    private void OnDestroy() {
        InputController.FireAction -= Attack;
    }

    public void RegisterPlayer() {
        GameManager manager = FindObjectOfType<GameManager>();
        if(manager.Player == null) {
            manager.Player = this;
        }
        else {
            Destroy(gameObject);
        }
    }

    private void Attack(string axis) {
        foreach(var weapon in m_Weapon) {
            weapon.SetDamage();
        }
    }
}
